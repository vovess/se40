import {Component, OnInit, Injectable, ViewEncapsulation} from '@angular/core';
import * as Rx from 'rxjs'
import {FormBuilder,FormsModule, FormControl, FormGroup, Validators} from '@angular/forms';
import * as JSZip from "jszip";
import * as FileSaver from 'file-saver';
import {saveAs} from "file-saver";
import {webSocket} from "rxjs/webSocket";
import {ChatService} from './chat.service';
import {WebsocketService} from "./web-socket.service";


//import {emit} from "cluster";

enum log_Mode {
  NONE = 'Log None',
  ALL = 'Log All',
  PASS = 'Log Pass',
  FAIL = 'Log Fail',
}
enum save_Mode {
  NONE = 'Save None',
  IMG = 'Save Images',
  STAT = 'Save Stats',
  BOTH = 'Save Both',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [WebsocketService,ChatService],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit{


  //TODO
  //figure out what needs to run immediately (eg check if pi is running)
  //create upload_model function
  //receive and download models from miller server
  //send data to miller server
  //get ip from pi

  formData=new FormData();
  fileLoaded = false;
  title = 'se40';
  passStat=0;
  failStat=0;
  passPercent=0;

  defaultIMG = new Image();

  imgArray = [new Image()];
  logSize=0;




  private message = {
    command: "STOP",
    data: "this is a test message"
  };
  constructor(private chat: ChatService) {
    this.chat.messages.subscribe((msg: any)=>{
      console.log(msg);

    })
  }
  ngOnInit() {
    //this.handleStats(95,5)

    this.defaultIMG.src = "assets/Log/mclogo.jpg"
    this.displayIMG = this.defaultIMG.src;
    this.sendMessage()
    console.log("test!!!!!")

  }
  sendMessage(){
    console.log("new message from client to websocket: ", this.message);
    this.chat.messages.next(this.message);
    this.message.command = "";
    this.message.data = "";
  }
  fileName:string = ""
  rawData:string[] = [];
  running:boolean = false;
  logMode: log_Mode = log_Mode.NONE;
  saveMode: save_Mode = save_Mode.NONE;
  displayIMG:string =""

  logModes: string[] = ['Log None', 'Log All', 'Log Pass', 'Log Fail'];
  saveModes:string[]=['Save None','Save Images','Save Stats','Save Both'];




  /*
  send FLUSH command to Pi
   */
  public flush() {
    //send flush command to pi
    // emit("flush","")
    this.imgArray = [];
    this.displayIMG = this.defaultIMG.src;
  }
  /*
  send START command to Pi
  Update Pi's config file
   */
  public start() {



    //emit("start","{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"START\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  },\n" +
    //   "  \"Data\": {\n" +
    //   "    \"Config\":[\n" +
    //   "      {\n" +
    //   "        \"LOG_MODE\": \"" +this.logMode + "\",\n" +
    //   "        \"SAVE_MODE\": \""+ this.saveMode+ "\",\n" +
    //   "        \"LOG_SIZE\": \""+ this.logSize + "\"\n" +
    //   "      }\n" +
    //   "      ]\n" +
    //   "  }\n" +
    //   "}")
    this.running = true;

  }
  public uploadPiModel(event:any){
    const file:File = event.target.files[0]
    if(file) {
      this.fileName = file.name;
      this.formData = new FormData();
      this.formData.append("model",file)
      this.fileLoaded = true;




    }

  }
  public sendModel(){
    //emit("UPLOAD_MODEL","{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"UPLOAD_MOD\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  },\n" +
    //   "  \"Data\": {\n" +
    //   "    \"File\":[\n" +
    //   "      {\n" +
    //   this.formData +
    //   "      }\n" +
    //   "      ]\n" +
    //   "  }\n" +
    //   "}")
  }

  /*
  sends STOP command to Pi
   */
  public stop() {

    // emit("stop","{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"STOP\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  }\n" +
    //   "}");
    this.running = false;
  }

  public downloadLog(){
    this.get_log();

    this.imgArray.forEach(img =>
    this.rawData.push(img.src)
    );

    const jszip = new JSZip();
    for(let i = 0; i < this.rawData.length; i++) {
      let binary = atob(this.rawData[i].split(',')[1]);
      let array = [];
      for (let j = 0; j < binary.length; j++) {
        array.push(binary.charCodeAt(j));
      }
      let image = new Blob([new Uint8Array(array)], {
        type: 'image/png'
      });
      jszip.file(`bottle${i}.png`, image)
      if (i === (this.rawData.length - 1)) {
        jszip.generateAsync({type: 'blob'}).then(function (content) {
          saveAs(content, 'LOG.zip');
        });
      }
    }


  }
  public handleStats(passNum:number,failNum:number){
    this.passStat = passNum;
    this.failStat = failNum;
    this.passPercent = ((Math.round(failNum/passNum*100)/100)-1)*-100
  }
  /*
  sends CAPTURE_SINGLE command to pi
   */
  public capture_single() {
    //emit("CAPTURE_SINGLE", "{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"CAPTURE_SINGLE\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  }\n" +
    //   "}" )

  }
  /*
  sends UPDATE_STATS command to pi
   */
  public update_stats() {
    // //emit("GET_STATS","{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"GET_STATS\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  }\n" +
    //   "}")
  }
  /*
  sends GET_log command to pi
   */
  public get_log() {
    // //emit('GET_LOG',"{\n" +
    //   "  \"Commands\": {\n" +
    //   "    \"command\": [\n" +
    //   "      {\n" +
    //   "        \"id\": \"GET_LOG\"\n" +
    //   "      }\n" +
    //   "    ]\n" +
    //   "  }\n" +
    //   "}")
    //emit("get_log","get_log data if any");
    // socket.on('get_log', data => {
    //   socket.broadcast.emit('update_message', username , message);
    //   console.log(username + ' just wrote ' + message);
    // });
  }
  public set_logMode(mode: string){

    if (mode === log_Mode.NONE){
      this.logMode = log_Mode.NONE;
    }
    if (mode === log_Mode.ALL){
      this.logMode = log_Mode.ALL;
    }
    if (mode === log_Mode.PASS){
      this.logMode = log_Mode.PASS;
    }
    if (mode === log_Mode.FAIL){
      this.logMode = log_Mode.FAIL;
    }



  }
  public set_saveMode(saveMode: string){
    if (saveMode === save_Mode.NONE){
      this.saveMode = save_Mode.NONE;
    }
    if (saveMode === save_Mode.IMG){
      this.saveMode = save_Mode.IMG;
    }
    if (saveMode === save_Mode.STAT){
      this.saveMode = save_Mode.STAT;
    }
    if (saveMode === save_Mode.BOTH){
      this.saveMode = save_Mode.BOTH;
    }

  }
  //TODO:
  //set up socket connection that listens for JSON data
  //Based on JSON data, call certain methods.
  /*
  download incoming log
   */

  public log(data:string){
    let byteStream = atob(data);
    let image = new Image();
    image.src ='data:image/png;base64,'+ byteStream;
    image.title= this.imgArray.length.toString();
    this.imgArray.push(image);
    this.displayIMG = image.src;
    this.update_stats();

  }



}
