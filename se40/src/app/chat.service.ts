import { Injectable } from "@angular/core";
import {map, Observable, Subject} from "rxjs";
import { WebsocketService } from "./web-socket.service";



const CHAT_URL = "ws://localhost:8080/";

export interface Message {
  command: string;
  data: string;
}

@Injectable()
export class ChatService {
  public messages: Subject<Message>;

  constructor(wsService: WebsocketService) {
    this.messages = <Subject<Message>>wsService.connect(CHAT_URL).pipe(map(
      (response: MessageEvent): Message => {
        let msg = JSON.parse(response.data);
        return {
          command: msg.command,
          data: msg.data
        };
      }
    ));
  }
}
