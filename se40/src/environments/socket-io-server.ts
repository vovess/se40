let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);

io.on('connection', function (socket: { emit: (arg0: string, arg1: string) => void; }) {
  console.log("A user connected");
  socket.emit('test event', 'THIS IS SOME NEW DATA!!!');
});

server.listen(8080, () => {

  console.log("Socket.io server is listening on port 3000");
});
